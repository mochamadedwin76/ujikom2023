@extends('layouts.pembimbing.dashboard')

@section('body')
    <table class="table">
        <thead>
            <tr>
                <th>nis</th>
                <th>nama</th>
                <th>file laporan PRAKERIN</th>
                <th>keterangan</th>
                <th>created at</th>
                <th>updated at</th>
                <th>Aksi</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($data as $item)
                <tr>
                    <td scope="row">{{ $item->nis }}</td>
                    <td scope="row">{{ $item->name }}</td>
                    <td scope="row"><a href="/download/{{ $item->laporan->file_laporan }}" class="btn btn-primary btn-sm">Download</a> </td>
                    <td scope="row">{{ $item->laporan->keterangan }}</td>
                    <td scope="row">{{ $item->laporan->created_at }}</td>
                    <td scope="row">{{ $item->laporan->updated_at }}</td>
                    <td scope="row">
                        <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo">Validasi</button>

                        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Validasi Laporan PRAKERIN</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <form action="/validasiLaporan" method="Post">
                                    @csrf
                                    <input type="hidden" name="id" value="{{ $item->laporan->id }}" >
                                <div class="form-group">
                                    <div class="mb-3">
                                      <label for="" class="form-label">Nama</label>
                                      <input type="text|password|email|number|submit|date|datetime|datetime-local|month|color|range|search|tel|time|url|week"
                                        class="form-control" value="{{ $item->name }}" id="" aria-describedby="helpId" placeholder="" readonly>
                                    </div>
                                    <label for="recipient-name" class="col-form-label">Keterangan:</label>
                                      <select class="form-control" name="keterangan" id="">
                                        <option value="">--pilih keterangan--</option>
                                        <option value="Perlu di Revisi">Perlu di Revisi</option>
                                        <option value="Laporan Siap di cetak">Laporan Siap di cetak</option>
                                      </select>
                                </div>
                                <div class="form-group">
                                    <label for="message-text" class="col-form-label">Message:</label>
                                    <textarea class="form-control" id="message-text" name="coment"></textarea>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </form>
                            </div>
                            </div>
                        </div>
                        </div>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    
@endsection