@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="card" >
            <div class="card-body">
                <h4 style="padding-top: 2px; ">selamat datang {{ Auth()->user()->name }} </h4>
                <form action="/upload/pengantar" method="POST" enctype="multipart/form-data">
                    @csrf
                    
                    <div class="form-group">
                        <div class="mb-3">
                            <input type="hidden" name="id" value="{{ Auth()->user()->id }}">
                            <input type="hidden" name="status" value="Menunggu Proses Validasi Surat Pengantar">
                          <label for="" class="form-label">Silahkan ajukan surat pengajuan/ pengantar Prakerin</label>
                          <input type="file"
                            class="form-control" name="file" id="" aria-describedby="helpId" placeholder="">
                        </div>
                    </div>

					<input type="submit" value="Upload" class="btn btn-primary">
				</form>
                <a href="/home" class="btn btn-danger">Back</a>
            </div>        
        </div>
    </div>

@endsection