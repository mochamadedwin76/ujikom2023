<?php

namespace App\Http\Controllers;

use App\Models\komputer;
use App\Models\user;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class KomputerController extends Controller
{
    public function index()
    {
        $data = User::all();
        return view('komputer.Index', ['data' => $data]);
    }
    public function tambah(Request $request)
    {
        DB::table('komputer')->insert([
            'name' => $request->name,
        ]);
        return redirect()->back();
    }
    public function edit($id)
    {
        $data = komputer::all()->where('id', $id);
        return view('komputer.edit', ['data' => $data]);
    }
    public function update(Request $request)
    {
        DB::table('komputer')->where('id', $request->id)->update([
            'name' => $request->name,
        ]);
        return redirect('/komputer');
    }
}
